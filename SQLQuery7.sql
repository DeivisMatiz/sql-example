--1
--Sukurkite funkcij�, kuri i�veda abonentus daugiausiai i�siuntusius/atsisiuntusius duomen� internetu (po vien� kiekvieno operatoriaus).
Create function dbo.udf_V71 ()
Returns Table
as
Return (Select d.operatoriausPavadinimas,ds.visoKiekisMB,ds.abonentoID
 from Operatorius as d 
 cross apply 
(Select top 1 Abonentas.abonentoID,sum(RysysInternetu.kiekisMB) as visoKiekisMB
from RysysInternetu
inner join Abonentas on RysysInternetu.abonentoID = Abonentas.abonentoID
inner join Planas on Abonentas.planoID = Planas.planoID
inner join Operatorius on Planas.operatoriausID = Operatorius.operatoriausID
where Operatorius.operatoriausPavadinimas = d.operatoriausPavadinimas
group by Abonentas.abonentoID
order by visoKiekisMB desc) as ds);

Select * from dbo.udf_V71();

Drop function udf_V71;

Select top 100 Abonentas.abonentoID, sum(RysysInternetu.kiekisMB) as visoKiekisMB,Operatorius.operatoriausPavadinimas
from RysysInternetu
inner join Abonentas on RysysInternetu.abonentoID = Abonentas.abonentoID
inner join Planas on Abonentas.planoID = Planas.planoID
inner join Operatorius on Planas.operatoriausID = Operatorius.operatoriausID
group by Abonentas.abonentoID, Operatorius.operatoriausPavadinimas
order by visoKiekisMB desc;

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

--2
--Sukurkite funkcij�, kuri i�veda abonentus, kurie tarpusavyje bendravo tam tikr� m�nes�, t.y. abonentas1 skambino ar ra�� SMS abonentui2 ir atvirk��iai.
Create function dbo.udf_V72 (@metai int, @menuo int)
Returns Table
as
Return (Select Abonentas.abonentoID, Abonentas.numeris, RysysP2P.adresatoNumeris, RysysP2P.rysioPradzia as bendravimoData, RysysP2P.paslaugosID
from Abonentas
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 1 and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo 
or RysysP2P.paslaugosID = 2 and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);

Select * from dbo.udf_V72(2013,2);

Drop function udf_V72;

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

--3
--Sukurkite funkcij�, kuri apskai�iuoja mokest� abonentui u� SMS tam tikr� m�nes�.
--Skai�iuojant mokest� �vertinti ir tai, kad galb�t yra u�sakytas lengvatinis planas.
Create function dbo.udf_V73_MAIN (@id int, @metai int, @menuo int)
Returns float
as
begin
return((Select dbo.udf_V73_kiekisVisoSms(@id, @metai, @menuo)) - (Select coalesce(dbo.udf_V73_kiekisSuLengvataSms(@id, @metai, @menuo),0)))*(Select coalesce(dbo.udf_V73_kainaBeLengvatosSms(@id, @metai, @menuo),0))
+ ((Select coalesce(dbo.udf_V73_kiekisSuLengvataPackSms(@id, @metai, @menuo),0))*(Select coalesce(dbo.udf_V73_kainaSuLengvataSms(@id, @metai, @menuo),0))) + (Select coalesce(dbo.udf_V73_abonentinisMokestisSmsL(@id, @metai, @menuo),0));
END;

Select dbo.udf_V73_MAIN(3098,2013,2) as moketinaSuma;

Drop function udf_V73_MAIN;

----------------------------------------------------------------------------------------------------------------------------

BEGIN--3SMS

Create function dbo.udf_V73_kiekisVisoSms (@id int, @metai int, @menuo int) 
Returns int
as
Begin
Return (Select count(Abonentas.abonentoID)
from Abonentas
left join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
left join UzsakytaLengvata on Abonentas.abonentoID = UzsakytaLengvata.abonentoID
left join UzsakomaLengvata on UzsakytaLengvata.komplektoID = UzsakomaLengvata.komplektoID
left join PlanoRinkinys on UzsakomaLengvata.rinkinioID = PlanoRinkinys.rinkinioID
where RysysP2P.paslaugosID = 2 and RysysP2P.abonentoID = @id and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);
END;

Select dbo.udf_V73_kiekisVisoSms(3098,2013,9);

Drop function udf_V73_kiekisVisoSms;

---------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V73_kainaBeLengvatosSms (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select PlanoRinkinys.kaina
from Abonentas
inner join Planas on Abonentas.planoID = Planas.planoID 
inner join PlanoRinkinys on Planas.planoID = PlanoRinkinys.planoID
where PlanoRinkinys.paslaugosID = 2 and Abonentas.abonentoID = @id);
END;

Select dbo.udf_V73_kainaBeLengvatosSms(3098,2013,7);

Drop function udf_V73_kainaBeLengvatosSms;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V73_kiekisSuLengvataSms (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select Count(Abonentas.abonentoID)
from UzsakomaLengvata
inner join UzsakytaLengvata on UzsakomaLengvata.komplektoID = UzsakytaLengvata.komplektoID 
inner join Abonentas on UzsakytaLengvata.abonentoID = Abonentas.abonentoID
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = @id and RysysP2P.rysioPradzia <= UzsakytaLengvata.atsisakymoData 
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo
and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData and UzsakytaLengvata.atsisakymoData is not null
or
RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = @id and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo
and UzsakytaLengvata.atsisakymoData is null);
END;

Select dbo.udf_V73_kiekisSuLengvataSms(3098,2013,7);

Drop function udf_V73_kiekisSuLengvataSms;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V73_kainaSuLengvataSms (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select distinct UzsakomaLengvata.kaina
from UzsakomaLengvata
inner join UzsakytaLengvata on UzsakomaLengvata.komplektoID = UzsakytaLengvata.komplektoID 
inner join Abonentas on UzsakytaLengvata.abonentoID = Abonentas.abonentoID
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = @id and RysysP2P.rysioPradzia <= UzsakytaLengvata.atsisakymoData 
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo
and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData and UzsakytaLengvata.atsisakymoData is not null
or
RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = @id and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo
and UzsakytaLengvata.atsisakymoData is null);
END;

Select dbo.udf_V73_kainaSuLengvataSms(3098,2013,7);

Drop function udf_V73_kainaSuLengvataSms;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V73_kiekisSuLengvataPackSms (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select Ceiling(cast(Count(Abonentas.abonentoID) as float)/UzsakomaLengvata.kiekis)
from UzsakomaLengvata
inner join UzsakytaLengvata on UzsakomaLengvata.komplektoID = UzsakytaLengvata.komplektoID 
inner join Abonentas on UzsakytaLengvata.abonentoID = Abonentas.abonentoID
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = @id and RysysP2P.rysioPradzia <= UzsakytaLengvata.atsisakymoData 
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo
and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData and UzsakytaLengvata.atsisakymoData is not null
or
RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = @id and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo
and UzsakytaLengvata.atsisakymoData is null
group by UzsakomaLengvata.kiekis);
END;

Select dbo.udf_V73_kiekisSuLengvataPackSms(3098,2013,7);

Drop function udf_V73_kiekisSuLengvataPackSms;

-----------------------------------------------------------------------------------------------------------------------------

Create function dbo.udf_V73_abonentinisMokestisSmsL (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select CASE
WHEN (dbo.udf_V74_MAINSMSL(@id,@metai,@menuo)) >= dbo.udf_V74_abonentinisMokestis(@id,@metai,@menuo)
THEN 0
ELSE dbo.udf_V74_abonentinisMokestis(@id,@metai,@menuo) - (dbo.udf_V74_MAINSMSL(@id,@metai,@menuo))
END);
END;

Select dbo.udf_V73_abonentinisMokestisSmsL(3098,2013,7);

Drop function udf_V73_abonentinisMokestisSmsL;

END--3SMS

BEGIN--TEST3

--kiekis zinuciu viso 
Select count(Abonentas.abonentoID)
from Abonentas
left join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
left join UzsakytaLengvata on Abonentas.abonentoID = UzsakytaLengvata.abonentoID
left join UzsakomaLengvata on UzsakytaLengvata.komplektoID = UzsakomaLengvata.komplektoID
left join PlanoRinkinys on UzsakomaLengvata.rinkinioID = PlanoRinkinys.rinkinioID
where RysysP2P.paslaugosID = 2 and RysysP2P.abonentoID = 1 and YEAR(RysysP2P.rysioPradzia) = 2013 and MONTH(RysysP2P.rysioPradzia) = 2;

--kaina be lengvatos uz zinute
Select PlanoRinkinys.kaina
from Abonentas
inner join Planas on Abonentas.planoID = Planas.planoID 
inner join PlanoRinkinys on Planas.planoID = PlanoRinkinys.planoID
where PlanoRinkinys.paslaugosID = 2 and Abonentas.abonentoID = 9;

--kiekis zinuciu su lengvata
Select Count(Abonentas.abonentoID)
from UzsakomaLengvata
inner join UzsakytaLengvata on UzsakomaLengvata.komplektoID = UzsakytaLengvata.komplektoID 
inner join Abonentas on UzsakytaLengvata.abonentoID = Abonentas.abonentoID
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = 5 and RysysP2P.rysioPradzia <= UzsakytaLengvata.atsisakymoData 
and YEAR(RysysP2P.rysioPradzia) = 2014 and MONTH(RysysP2P.rysioPradzia) = 7
and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData and UzsakytaLengvata.atsisakymoData is not null
or
RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = 5 and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData
and YEAR(RysysP2P.rysioPradzia) = 2014 and MONTH(RysysP2P.rysioPradzia) = 7
and UzsakytaLengvata.atsisakymoData is null

--kaina su lengvata
Select distinct UzsakomaLengvata.kaina
from UzsakomaLengvata
inner join UzsakytaLengvata on UzsakomaLengvata.komplektoID = UzsakytaLengvata.komplektoID 
inner join Abonentas on UzsakytaLengvata.abonentoID = Abonentas.abonentoID
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = 3098 and RysysP2P.rysioPradzia <= UzsakytaLengvata.atsisakymoData 
and YEAR(RysysP2P.rysioPradzia) = 2013 and MONTH(RysysP2P.rysioPradzia) = 7
and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData and UzsakytaLengvata.atsisakymoData is not null
or
RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = 3098 and RysysP2P.rysioPradzia >= UzsakytaLengvata.uzsakymoData
and YEAR(RysysP2P.rysioPradzia) = 2013 and MONTH(RysysP2P.rysioPradzia) = 7
and UzsakytaLengvata.atsisakymoData is null

Select * from UzsakomaLengvata

Select *
from UzsakytaLengvata
where abonentoID = 3098

Select *
from RysysP2P
where abonentoID = 1 and paslaugosID = 2 and YEAR(RysysP2P.rysioPradzia) = 2013 and MONTH(RysysP2P.rysioPradzia) = 2

Select * from PlanoRinkinys where paslaugosID = 2

Select * 
from Planas
inner join Abonentas on Planas.planoID = Abonentas.planoID
where Abonentas.abonentoID = 1

select * from abonentas where abonentoID = 1

--9id turi moketi uz sms. mazesni id ne, tai negalima pratestuoti

END--TEST3

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

--4
--Sukurkite funkcij�, kuri apskai�iuoja bendr� tam tikro m�nesio abonento s�skait� u� visas suteiktas paslaugas.
--Skai�iuojant mok�tin� sum� �vertinti visas u�sakomasias lengvatas, galiojan�ias tam laikotarpiui.
--Jei lengvata galiojo pusei (ketvirtis) m�nesio, tai taikoma tik pus� (ketvirtoji) lengvatos dalies.
--Funkcija turi i�vesti ne tik galutin� sum�, bet ir dedamasias jos dalis (skambu�iai, SMS, internetas ir t.t.).
Create function dbo.udf_V74_MAIN (@id int, @metai int, @menuo int)
Returns table
as
return
(Select dbo.udf_V74_MAINTOTAL(@id, @metai, @menuo) as sumaViso,
(Select coalesce(dbo.udf_V74_MAINSMSL(@id, @metai, @menuo),0)) as sumaUzSmsLietuvoje,
(Select coalesce(dbo.udf_V74_MAINNET(@id, @metai, @menuo),0)) as sumaUzInterneta,
(Select coalesce(dbo.udf_V74_MAINPP(@id, @metai, @menuo),0)) as sumaUzPapPaslauga,
(Select coalesce(dbo.udf_V74_MAINSMSU(@id, @metai, @menuo),0)) as sumaUzSmsIUzsieni,
(Select coalesce(dbo.udf_V74_MAINMMS(@id, @metai, @menuo),0)) as sumaUzMms,
(Select coalesce(dbo.udf_V74_MAINSL(@id, @metai, @menuo),0)) as sumaUzSkambLietuvoje,
(Select coalesce(dbo.udf_V74_MAINSV(@id, @metai, @menuo),0)) as sumaUzVaizdoSkamb,
(Select coalesce(dbo.udf_V74_MAINSU(@id, @metai, @menuo),0)) as sumaUzSkambIUzsieni,
(Select coalesce(dbo.udf_V74_MAINABON(@id, @metai, @menuo),0)) as sumaUzAbonentiniMok
);

Select * from dbo.udf_V74_MAIN(349,2013,7);

Drop function udf_V74_MAIN;

---------------------------------------------------------

BEGIN--SUBMAIN

Create function dbo.udf_V74_MAINTOTAL (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
(
(Select coalesce(dbo.udf_V74_MAINSMSL(@id, @metai, @menuo),0))
+(Select coalesce(dbo.udf_V74_MAINNET(@id, @metai, @menuo),0))
+(Select coalesce(dbo.udf_V74_MAINPP(@id, @metai, @menuo),0))
+(Select coalesce(dbo.udf_V74_MAINSMSU(@id, @metai, @menuo),0))
+(Select coalesce(dbo.udf_V74_MAINMMS(@id, @metai, @menuo),0))
+(Select coalesce(dbo.udf_V74_MAINSL(@id, @metai, @menuo),0))
+(Select coalesce(dbo.udf_V74_MAINSV(@id, @metai, @menuo),0))
+(Select coalesce(dbo.udf_V74_MAINSU(@id, @metai, @menuo),0))
+(Select coalesce(dbo.udf_V74_MAINABON(@id, @metai, @menuo),0))
);
END;

Select dbo.udf_V74_MAINTOTAL(349,2013,7);

Drop function udf_V74_MAINTOTAL;

---------------------------------------------------------

Create function dbo.udf_V74_MAINSMSL (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
((Select coalesce(dbo.udf_V73_kiekisVisoSms(@id, @metai, @menuo),0)) - (Select coalesce(dbo.udf_V73_kiekisSuLengvataSms(@id, @metai, @menuo),0)))
*(Select coalesce(dbo.udf_V73_kainaBeLengvatosSms(@id, @metai, @menuo),0))
+ (Select coalesce(dbo.udf_V73_kiekisSuLengvataPackSms(@id, @metai, @menuo),0))*(Select coalesce(dbo.udf_V74_laikotarpioLengvatosKainaSms(@id, @metai, @menuo),0));
END;

Select dbo.udf_V74_MAINSMSL(349,2013,7);

Drop function udf_V74_MAINSMSL;

---------------------------------------------------------
Create function dbo.udf_V74_MAINNET (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
((Select coalesce(dbo.udf_V74_kiekisVisoNet(@id, @metai, @menuo),0)) - (Select coalesce(dbo.udf_V74_kiekisSuLengvataNet(@id, @metai, @menuo),0)))
*(Select coalesce(dbo.udf_V74_kainaBeLengvatosNet(@id, @metai, @menuo),0))
+ (Select coalesce(dbo.udf_V74_kiekisSuLengvataPackNet(@id, @metai, @menuo),0))*(Select coalesce(dbo.udf_V74_laikotarpioLengvatosKainaNet(@id, @metai, @menuo),0));
END;

Select dbo.udf_V74_MAINNET(12,2014,6);

Drop function udf_V74_MAINNET;

---------------------------------------------------------
Create function dbo.udf_V74_MAINPP (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
(Select coalesce(dbo.udf_V74_kainaSuLengvataPaslauga(@id, @metai, @menuo),0));
END;

Select dbo.udf_V74_MAINPP(12,2014,6);

Drop function udf_V74_MAINPP;

---------------------------------------------------------
Create function dbo.udf_V74_MAINSMSU (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
((Select coalesce(dbo.udf_V74_kiekisVisoSmsU(@id, @metai, @menuo),0))*(Select coalesce(dbo.udf_V74_kainaSmsU(@id, @metai, @menuo),0)));
END;

Select dbo.udf_V74_MAINSMSU(12,2014,6);

Drop function udf_V74_MAINSMSU;

---------------------------------------------------------
Create function dbo.udf_V74_MAINMMS (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
((Select coalesce(dbo.udf_V74_kiekisVisoMms(@id, @metai, @menuo),0))*(Select coalesce(dbo.udf_V74_kainaMms(@id, @metai, @menuo),0)));
END;

Select dbo.udf_V74_MAINMMS(12,2014,6);

Drop function udf_V74_MAINMMS;

---------------------------------------------------------
Create function dbo.udf_V74_MAINSL (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
((Select coalesce(dbo.udf_V74_trukmeVisoSkambuciaiL(@id, @metai, @menuo),0))*(Select coalesce(dbo.udf_V74_kainaSkambuciaiL(@id, @metai, @menuo),0)));
END;

Select dbo.udf_V74_MAINSL(12,2014,6);

Drop function udf_V74_MAINSL;

---------------------------------------------------------
Create function dbo.udf_V74_MAINSV (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
((Select coalesce(dbo.udf_V74_kainaSkambuciaiV(@id, @metai, @menuo),0))*(Select coalesce(dbo.udf_V74_trukmeVisoSkambuciaiV(@id, @metai, @menuo),0)));
END;

Select dbo.udf_V74_MAINSV(1,2013,7);

Drop function udf_V74_MAINSV;

---------------------------------------------------------
Create function dbo.udf_V74_MAINSU (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
(Select coalesce(dbo.udf_V74_sumaVisoSkambuciaiU(@id, @metai, @menuo),0));
END;

Select dbo.udf_V74_MAINSU(349,2013,7);

Drop function udf_V74_MAINSU;

---------------------------------------------------------
Create function dbo.udf_V74_MAINABON (@id int, @metai int, @menuo int)
Returns float
as
Begin
return
(Select coalesce(dbo.udf_V74_abonentinisMokestisViso(@id, @metai, @menuo),0));
END;

Select dbo.udf_V74_MAINABON(3098,2013,7);

Drop function udf_V74_MAINABON;

---------------------------------------------------------

END--SUBMAIN


BEGIN--SMSL

Create function dbo.udf_V74_lengvatosPabaigaSms(@id int, @metai int, @menuo int)
Returns date
as
begin
return(
Select top 1 RysysP2P.rysioPradzia
from UzsakomaLengvata
inner join UzsakytaLengvata on UzsakomaLengvata.komplektoID = UzsakytaLengvata.komplektoID 
inner join Abonentas on UzsakytaLengvata.abonentoID = Abonentas.abonentoID
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 2 and Abonentas.abonentoID = @id 
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo and RysysP2P.rysioPradzia <= UzsakytaLengvata.atsisakymoData
order by RysysP2P.rysioPradzia desc);
END;

Select dbo.udf_V74_lengvatosPabaigaSms(8,2014,8);

Drop function udf_V74_lengvatosPabaigaSms;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_laikotarpioLengvatosKainaSms(@id int, @metai int, @menuo int)
Returns float
as
begin
return(
Select CASE
WHEN DAY((Select dbo.udf_V74_lengvatosPabaigaSms(@id,@metai,@menuo))) <= 8 THEN (Select coalesce(dbo.udf_V73_kainaSuLengvataSms(@id,@metai,@menuo),0)/4)
WHEN DAY((Select dbo.udf_V74_lengvatosPabaigaSms(@id,@metai,@menuo))) > 8 and DAY((Select dbo.udf_V74_lengvatosPabaigaSms(@id,@metai,@menuo))) <= 16 
THEN (Select coalesce(dbo.udf_V73_kainaSuLengvataSms(@id,@metai,@menuo),0)/2)
ELSE (Select coalesce(dbo.udf_V73_kainaSuLengvataSms(@id,@metai,@menuo),0))
END
);
END;

Select dbo.udf_V74_laikotarpioLengvatosKainaSms(8,2014,8);

Drop function udf_V74_laikotarpioLengvatosKainaSms;


END--SMSL


BEGIN--NET

Create function dbo.udf_V74_lengvatosPabaigaNet(@id int, @metai int, @menuo int)
Returns date
as
begin
return(
Select top 1 RysysInternetu.rysioPabaiga
from Abonentas
inner join RysysInternetu on Abonentas.abonentoID = RysysInternetu.abonentoID
inner join UzsakytasInternetoPlanas on Abonentas.abonentoID = UzsakytasInternetoPlanas.abonentoID
inner join InternetoPlanas on UzsakytasInternetoPlanas.internetoPlanoID = InternetoPlanas.internetoPlanoID
where Abonentas.abonentoID = @id and YEAR(RysysInternetu.rysioPabaiga) = @metai and MONTH(RysysInternetu.rysioPabaiga) = @menuo 
and RysysInternetu.rysioPabaiga <= UzsakytasInternetoPlanas.atsisakymoData
order by RysysInternetu.rysioPabaiga desc);
END;

Select dbo.udf_V74_lengvatosPabaigaNet(12,2014,6);

Drop function udf_V74_lengvatosPabaigaNet;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_laikotarpioLengvatosKainaNet(@id int, @metai int, @menuo int)
Returns float
as
begin
return(
Select CASE
WHEN DAY((Select dbo.udf_V74_lengvatosPabaigaNet(@id,@metai,@menuo))) <= 8 THEN (Select coalesce(dbo.udf_V74_kainaSuLengvataNet(@id,@metai,@menuo),0)/4)
WHEN DAY((Select dbo.udf_V74_lengvatosPabaigaNet(@id,@metai,@menuo))) > 8 and DAY((Select dbo.udf_V74_lengvatosPabaigaNet(@id,@metai,@menuo))) <= 16 
THEN (Select coalesce(dbo.udf_V74_kainaSuLengvataNet(@id,@metai,@menuo),0)/2)
ELSE (Select coalesce(dbo.udf_V74_kainaSuLengvataNet(@id,@metai,@menuo),0))
END
);
END;

Select dbo.udf_V74_laikotarpioLengvatosKainaNet(12,2014,6);

Drop function udf_V74_laikotarpioLengvatosKainaNet;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_kiekisVisoNet (@id int, @metai int, @menuo int) 
Returns float
as
Begin
Return (Select sum(RysysInternetu.kiekisMB)
from Abonentas
inner join RysysInternetu on Abonentas.abonentoID = RysysInternetu.abonentoID
inner join UzsakytasInternetoPlanas on Abonentas.abonentoID = UzsakytasInternetoPlanas.abonentoID
inner join InternetoPlanas on UzsakytasInternetoPlanas.internetoPlanoID = InternetoPlanas.internetoPlanoID
where Abonentas.abonentoID = @id and YEAR(RysysInternetu.rysioPabaiga) = @metai and MONTH(RysysInternetu.rysioPabaiga) = @menuo);
END;

Select dbo.udf_V74_kiekisVisoNet(12,2014,6);

Drop function udf_V74_kiekisVisoNet;

Select * from RysysInternetu where abonentoID = 12 and year(rysioPabaiga) = 2014 and MONTH(rysioPabaiga) = 6;

---------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_kainaBeLengvatosNet (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select InternetoPlanas.papildomaKaina
from Abonentas
inner join UzsakytasInternetoPlanas on Abonentas.abonentoID = UzsakytasInternetoPlanas.abonentoID 
inner join InternetoPlanas on UzsakytasInternetoPlanas.internetoPlanoID = InternetoPlanas.internetoPlanoID
where Abonentas.abonentoID = @id);
END;

Select dbo.udf_V74_kainaBeLengvatosNet(12,2014,6);

Drop function udf_V74_kainaBeLengvatosNet;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_kiekisSuLengvataNet (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select sum(RysysInternetu.kiekisMB)
from Abonentas
inner join RysysInternetu on Abonentas.abonentoID = RysysInternetu.abonentoID
inner join UzsakytasInternetoPlanas on Abonentas.abonentoID = UzsakytasInternetoPlanas.abonentoID
inner join InternetoPlanas on UzsakytasInternetoPlanas.internetoPlanoID = InternetoPlanas.internetoPlanoID
where Abonentas.abonentoID = @id and RysysInternetu.rysioPabaiga <= UzsakytasInternetoPlanas.atsisakymoData 
and YEAR(RysysInternetu.rysioPabaiga) = @metai and MONTH(RysysInternetu.rysioPabaiga) = @menuo
and RysysInternetu.rysioPabaiga >= UzsakytasInternetoPlanas.uzsakymoData and UzsakytasInternetoPlanas.atsisakymoData is not null
or
Abonentas.abonentoID = @id and RysysInternetu.rysioPabaiga >= UzsakytasInternetoPlanas.uzsakymoData
and YEAR(RysysInternetu.rysioPabaiga) = @metai and MONTH(RysysInternetu.rysioPabaiga) = @menuo
and UzsakytasInternetoPlanas.atsisakymoData is null);
END;

Select dbo.udf_V74_kiekisSuLengvataNet(12,2014,6);

Drop function udf_V74_kiekisSuLengvataNet;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_kiekisSuLengvataPackNet (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select Ceiling(cast(sum(RysysInternetu.kiekisMB) as float)/InternetoPlanas.kiekisMB)
from Abonentas
inner join RysysInternetu on Abonentas.abonentoID = RysysInternetu.abonentoID
inner join UzsakytasInternetoPlanas on Abonentas.abonentoID = UzsakytasInternetoPlanas.abonentoID
inner join InternetoPlanas on UzsakytasInternetoPlanas.internetoPlanoID = InternetoPlanas.internetoPlanoID
where Abonentas.abonentoID = @id and RysysInternetu.rysioPabaiga <= UzsakytasInternetoPlanas.atsisakymoData 
and YEAR(RysysInternetu.rysioPabaiga) = @metai and MONTH(RysysInternetu.rysioPabaiga) = @menuo
and RysysInternetu.rysioPabaiga >= UzsakytasInternetoPlanas.uzsakymoData and UzsakytasInternetoPlanas.atsisakymoData is not null
or
Abonentas.abonentoID = @id and RysysInternetu.rysioPabaiga >= UzsakytasInternetoPlanas.uzsakymoData
and YEAR(RysysInternetu.rysioPabaiga) = @metai and MONTH(RysysInternetu.rysioPabaiga) = @menuo
and UzsakytasInternetoPlanas.atsisakymoData is null
group by InternetoPlanas.kiekisMB);
END;

Select dbo.udf_V74_kiekisSuLengvataPackNet(12,2014,6);

Drop function udf_V74_kiekisSuLengvataPackNet;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_kainaSuLengvataNet (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select distinct InternetoPlanas.kaina
from Abonentas
inner join RysysInternetu on Abonentas.abonentoID = RysysInternetu.abonentoID
inner join UzsakytasInternetoPlanas on Abonentas.abonentoID = UzsakytasInternetoPlanas.abonentoID
inner join InternetoPlanas on UzsakytasInternetoPlanas.internetoPlanoID = InternetoPlanas.internetoPlanoID
where Abonentas.abonentoID = @id and RysysInternetu.rysioPabaiga <= UzsakytasInternetoPlanas.atsisakymoData 
and YEAR(RysysInternetu.rysioPabaiga) = @metai and MONTH(RysysInternetu.rysioPabaiga) = @menuo
and RysysInternetu.rysioPabaiga >= UzsakytasInternetoPlanas.uzsakymoData and UzsakytasInternetoPlanas.atsisakymoData is not null
or
Abonentas.abonentoID = @id and RysysInternetu.rysioPabaiga >= UzsakytasInternetoPlanas.uzsakymoData
and YEAR(RysysInternetu.rysioPabaiga) = @metai and MONTH(RysysInternetu.rysioPabaiga) = @menuo
and UzsakytasInternetoPlanas.atsisakymoData is null
group by InternetoPlanas.kaina);
END;

Select dbo.udf_V74_kainaSuLengvataNet(12,2014,6);

Drop function udf_V74_kainaSuLengvataNet;

END--NET


BEGIN--PP

Create function dbo.udf_V74_kainaSuLengvataPaslauga(@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select OperatoriausPapildomaPaslauga.kaina
from Abonentas
inner join UzsakytaPapildomaPaslauga on Abonentas.abonentoID = UzsakytaPapildomaPaslauga.abonentoID
inner join OperatoriausPapildomaPaslauga on UzsakytaPapildomaPaslauga.oprPapildomosPslgsID = OperatoriausPapildomaPaslauga.oprPapildomosPslgsID
where Abonentas.abonentoID = @id and UzsakytaPapildomaPaslauga.uzsakymoData <= Abonentas.sutartiesPabaiga 
and YEAR(UzsakytaPapildomaPaslauga.uzsakymoData) = @metai and MONTH(UzsakytaPapildomaPaslauga.uzsakymoData) = @menuo
and UzsakytaPapildomaPaslauga.uzsakymoData >= Abonentas.sutartiesPradzia and Abonentas.sutartiesPabaiga is not null
or
Abonentas.abonentoID = @id and UzsakytaPapildomaPaslauga.uzsakymoData >= Abonentas.sutartiesPradzia
and YEAR(UzsakytaPapildomaPaslauga.uzsakymoData) = @metai and MONTH(UzsakytaPapildomaPaslauga.uzsakymoData) = @menuo
and Abonentas.sutartiesPabaiga is null
);
END;

Select dbo.udf_V74_kainaSuLengvataPaslauga(12,2014,6);

Drop function udf_V74_kainaSuLengvataPaslauga;

END--PP


BEGIN--SMSU

Create function dbo.udf_V74_kiekisVisoSmsU (@id int, @metai int, @menuo int) 
Returns int
as
Begin
Return (Select count(Abonentas.abonentoID)
from Abonentas
left join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
left join UzsakytaLengvata on Abonentas.abonentoID = UzsakytaLengvata.abonentoID
left join UzsakomaLengvata on UzsakytaLengvata.komplektoID = UzsakomaLengvata.komplektoID
left join PlanoRinkinys on UzsakomaLengvata.rinkinioID = PlanoRinkinys.rinkinioID
where RysysP2P.paslaugosID = 5 and RysysP2P.abonentoID = @id and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);
END;

Select dbo.udf_V74_kiekisVisoSmsU(3098,2013,7);

Drop function udf_V74_kiekisVisoSmsU;

---------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_kainaSmsU (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select PlanoRinkinys.kaina
from Abonentas
inner join Planas on Abonentas.planoID = Planas.planoID 
inner join PlanoRinkinys on Planas.planoID = PlanoRinkinys.planoID
where PlanoRinkinys.paslaugosID = 5 and Abonentas.abonentoID = @id);
END;

Select dbo.udf_V74_kainaSmsU(3098,2013,7);

Drop function udf_V74_kainaSmsU;

END--SMSU


BEGIN--MMS

Create function dbo.udf_V74_kiekisVisoMms (@id int, @metai int, @menuo int) 
Returns int
as
Begin
Return (Select count(Abonentas.abonentoID)
from Abonentas
left join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
left join UzsakytaLengvata on Abonentas.abonentoID = UzsakytaLengvata.abonentoID
left join UzsakomaLengvata on UzsakytaLengvata.komplektoID = UzsakomaLengvata.komplektoID
left join PlanoRinkinys on UzsakomaLengvata.rinkinioID = PlanoRinkinys.rinkinioID
where RysysP2P.paslaugosID = 3 and RysysP2P.abonentoID = @id and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);
END;

Select dbo.udf_V74_kiekisVisoMms(3098,2013,7);

Drop function udf_V74_kiekisVisoMms;

---------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_kainaMms (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select PlanoRinkinys.kaina
from Abonentas
inner join Planas on Abonentas.planoID = Planas.planoID 
inner join PlanoRinkinys on Planas.planoID = PlanoRinkinys.planoID
where PlanoRinkinys.paslaugosID = 3 and Abonentas.abonentoID = @id);
END;

Select dbo.udf_V74_kainaMms(3098,2013,7);

Drop function udf_V74_kainaMms;

END--MMS


BEGIN--SL

Create function dbo.udf_V74_trukmeVisoSkambuciaiL (@id int, @metai int, @menuo int) 
Returns int
as
Begin
Return (Select sum(ceiling(cast(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)as float)/60))
from Abonentas
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 1 and RysysP2P.abonentoID = @id and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);
END;

Select dbo.udf_V74_trukmeVisoSkambuciaiL(12,2014,6);

Drop function udf_V74_trukmeVisoSkambuciaiL;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_kainaSkambuciaiL (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select PlanoRinkinys.kaina
from Abonentas
inner join Planas on Abonentas.planoID = Planas.planoID
inner join PlanoRinkinys on Planas.planoID = PlanoRinkinys.planoID
where PlanoRinkinys.paslaugosID = 1 and Abonentas.abonentoID = @id);
END;

Select dbo.udf_V74_kainaSkambuciaiL(12,2014,6);

Drop function udf_V74_kainaSkambuciaiL;

END--SL


BEGIN--SV

Create function dbo.udf_V74_kainaSkambuciaiV (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select PlanoRinkinys.kaina
from Abonentas
inner join Planas on Abonentas.planoID = Planas.planoID
inner join PlanoRinkinys on Planas.planoID = PlanoRinkinys.planoID
where PlanoRinkinys.paslaugosID = 6 and Abonentas.abonentoID = @id);
END;

Select dbo.udf_V74_kainaSkambuciaiV(1,2013,7);

Drop function udf_V74_kainaSkambuciaiV;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_trukmeVisoSkambuciaiV (@id int, @metai int, @menuo int) 
Returns int
as
Begin
Return (Select sum(ceiling(cast(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)as float)/60))
from Abonentas
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 6 and RysysP2P.abonentoID = @id and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);
END;

Select dbo.udf_V74_trukmeVisoSkambuciaiV(1,2013,7);

Drop function udf_V74_trukmeVisoSkambuciaiV;

END--SV


BEGIN--SU

Create function dbo.udf_V74_trukmeVisoSkambuciaiU (@id int, @metai int, @menuo int) 
Returns table
as
Return (Select ceiling(cast(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)as float)/60) as skambuciuMinutes
from RysysP2P
where RysysP2P.paslaugosID = 4 and RysysP2P.abonentoID = @id and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);

Select * from dbo.udf_V74_trukmeVisoSkambuciaiU(349,2013,2);

Drop function udf_V74_trukmeVisoSkambuciaiU;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_kainosVisoSkambuciaiU (@id int, @metai int, @menuo int) 
Returns table
as
Return (Select PaslaugosKainaZonoje.kaina
from RysysP2P 
inner join Salis on RysysP2P.adresatoSalis = Salis.saliesKodas
inner join Zona on Salis.zonosID = Zona.zonosID
inner join PaslaugosKainaZonoje on Zona.zonosID = PaslaugosKainaZonoje.zonosID
inner join Operatorius on PaslaugosKainaZonoje.operatoriausID = Operatorius.operatoriausID
inner join Planas on Operatorius.operatoriausID = Planas.operatoriausID
inner join Abonentas on Planas.planoID = Abonentas.planoID
where RysysP2P.paslaugosID = 4 and Abonentas.abonentoID = @id and RysysP2P.abonentoID = @id
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);

Select * from dbo.udf_V74_kainosVisoSkambuciaiU(349,2013,7);

Drop function udf_V74_kainosVisoSkambuciaiU;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_sumosVisoSkambuciaiU (@id int, @metai int, @menuo int) 
Returns table
as
Return (Select PaslaugosKainaZonoje.kaina * ceiling(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)/60) as skambuciuKainos
from RysysP2P 
inner join Salis on RysysP2P.adresatoSalis = Salis.saliesKodas
inner join Zona on Salis.zonosID = Zona.zonosID
inner join PaslaugosKainaZonoje on Zona.zonosID = PaslaugosKainaZonoje.zonosID
inner join Operatorius on PaslaugosKainaZonoje.operatoriausID = Operatorius.operatoriausID
inner join Planas on Operatorius.operatoriausID = Planas.operatoriausID
inner join Abonentas on Planas.planoID = Abonentas.planoID
where RysysP2P.paslaugosID = 4 and Abonentas.abonentoID = @id and RysysP2P.abonentoID = @id
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);

Select * from dbo.udf_V74_sumosVisoSkambuciaiU(349,2013,7);

Drop function udf_V74_sumosVisoSkambuciaiU;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_sumaVisoSkambuciaiU(@id int, @metai int, @menuo int) 
Returns float
as
Begin
Return (Select sum(skambuciuKainos) from dbo.udf_V74_sumosVisoSkambuciaiU(@id, @metai, @menuo));
End;

Select dbo.udf_V74_sumaVisoSkambuciaiU(349,2013,7)  as sumaUzSkambuciusIUzsieni;

Drop function udf_V74_sumaVisoSkambuciaiU;

END--SU


BEGIN--ABON

Create function dbo.udf_V74_abonentinisMokestis (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select distinct Planas.kaina
from Abonentas
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
inner join Planas on Abonentas.planoID = Planas.planoID
where RysysP2P.paslaugosID = 1 and RysysP2P.abonentoID = @id 
and YEAR(RysysP2P.rysioPradzia) = @metai and MONTH(RysysP2P.rysioPradzia) = @menuo);
END;

Select dbo.udf_V74_abonentinisMokestis(3098,2013,7);

Drop function udf_V74_abonentinisMokestis;

-----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V74_abonentinisMokestisViso (@id int,@metai int,@menuo int)
Returns float
as
Begin
Return (Select CASE
WHEN (dbo.udf_V74_MAINSL(@id,@metai,@menuo)+dbo.udf_V74_MAINSMSL(@id,@metai,@menuo)) >= dbo.udf_V74_abonentinisMokestis(@id,@metai,@menuo)
THEN 0
ELSE dbo.udf_V74_abonentinisMokestis(@id,@metai,@menuo) - (dbo.udf_V74_MAINSL(@id,@metai,@menuo)+dbo.udf_V74_MAINSMSL(@id,@metai,@menuo))
END);
END;

Select dbo.udf_V74_abonentinisMokestisViso(3098,2013,7);

Drop function udf_V74_abonentinisMokestisViso;

END--ABON


BEGIN--TEST4

Select PlanoRinkinys.kaina, planoID from PlanoRinkinys where paslaugosID = 6

Select * from UzsakomaLengvata
Select * from PlanoRinkinys where paslaugosID = 1

Select *
from UzsakytaLengvata
where abonentoID = 3098

----------------------------------------------------------------------------------------------------------------------------
Select *
from RysysP2P
where abonentoID = 1 and paslaugosID = 6 and YEAR(RysysP2P.rysioPradzia) = 2013 and MONTH(RysysP2P.rysioPradzia) = 7

Select * from PlanoRinkinys where paslaugosID = 5

Select *
from RysysP2P
where abonentoID = 349 and paslaugosID = 4 and YEAR(RysysP2P.rysioPradzia) = 2013 and MONTH(RysysP2P.rysioPradzia) = 7

----------------------------------------------------------------------------------------------------------------------------
Select * from InternetoPlanas

Select * from UzsakytasInternetoPlanas where abonentoID = 12;

----------------------------------------------------------------------------------------------------------------------------
Select * from Abonentas where abonentoID = 12;
Select * from UzsakytaLengvata where abonentoID = 12;
Select * from UzsakytasInternetoPlanas where abonentoID = 12;
Select * from UzsakytaPapildomaPaslauga where abonentoID = 12;

----------------------------------------------------------------------------------------------------------------------------
Select distinct Planas.kaina
from Abonentas
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
inner join Planas on Abonentas.planoID = Planas.planoID
where RysysP2P.paslaugosID = 1 and RysysP2P.abonentoID = 3098 
and YEAR(RysysP2P.rysioPradzia) = 2013 and MONTH(RysysP2P.rysioPradzia) = 7

select * from planas
inner join Abonentas on Planas.planoID = Abonentas.planoID
where Abonentas.abonentoID = 3098 

----------------------------------------------------------------------------------------------------------------------------
Select PaslaugosKainaZonoje.kaina
from RysysP2P 
inner join Salis on RysysP2P.adresatoSalis = Salis.saliesKodas
inner join Zona on Salis.zonosID = Zona.zonosID
inner join PaslaugosKainaZonoje on Zona.zonosID = PaslaugosKainaZonoje.zonosID
inner join Operatorius on PaslaugosKainaZonoje.operatoriausID = Operatorius.operatoriausID
inner join Planas on Operatorius.operatoriausID = Planas.operatoriausID
inner join Abonentas on Planas.planoID = Abonentas.planoID
where RysysP2P.paslaugosID = 4 and Abonentas.abonentoID = 349 and RysysP2P.abonentoID = 349
and YEAR(RysysP2P.rysioPradzia) = 2013 and MONTH(RysysP2P.rysioPradzia) = 7

END--TEST4

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

--5
--Sukurkite proced�r�, kuri automati�kai prat�sia sutart� abonentui tomis pa�iomis s�lygomis, kokios buvo anks�iau (trukm�, papildomos paslaugos ir t.t.).
Create proc sp_pratesimas
@id int
as
begin
update Abonentas set Abonentas.sutartiesPabaiga = DATEADD(DAY,DATEDIFF(DAY,Abonentas.sutartiesPradzia, Abonentas.sutartiesPabaiga),Abonentas.sutartiesPabaiga) where Abonentas.abonentoID = @id;
update UzsakytaLengvata set UzsakytaLengvata.atsisakymoData = DATEADD(DAY,DATEDIFF(DAY,UzsakytaLengvata.uzsakymoData, UzsakytaLengvata.atsisakymoData),UzsakytaLengvata.atsisakymoData) where UzsakytaLengvata.abonentoID = @id;
update UzsakytasInternetoPlanas set UzsakytasInternetoPlanas.atsisakymoData = DATEADD(DAY,DATEDIFF(DAY,UzsakytasInternetoPlanas.uzsakymoData, UzsakytasInternetoPlanas.atsisakymoData),UzsakytasInternetoPlanas.atsisakymoData) where UzsakytasInternetoPlanas.abonentoID = @id;
update UzsakytaPapildomaPaslauga set UzsakytaPapildomaPaslauga.atsisakymoData = DATEADD(DAY,DATEDIFF(DAY,UzsakytaPapildomaPaslauga.uzsakymoData, UzsakytaPapildomaPaslauga.atsisakymoData),UzsakytaPapildomaPaslauga.atsisakymoData) where UzsakytaPapildomaPaslauga.abonentoID = @id;
end;

Execute sp_pratesimas 2;

Drop proc sp_pratesimas;

Select * from Abonentas where abonentoID = 2;
Select * from UzsakytaLengvata where abonentoID = 2;
Select * from UzsakytasInternetoPlanas where abonentoID = 2;
Select * from UzsakytaPapildomaPaslauga where abonentoID = 2;

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

--6
--Sukurkite funkcij�, pasi�lan�i� abonentui geriausiai jam tinkant� mok�jimo plan�, �vertinant paskutini� trij� m�nesi� naudojimosi paslaugomis tendencijas.
Create function dbo.udf_V76_MAIN (@id int)
Returns nvarchar(100)
as
begin
return(Select Case
when Max(paslaugosKiekiai) = coalesce(dbo.udf_V76_kiekis3MenSms(@id),0) then 
(Select Operatorius.operatoriausPavadinimas + Planas.planoPavadinimas from Planas 
inner join Operatorius on Planas.operatoriausID = Operatorius.operatoriausID 
where Planas.planoID = 2 and Max(paslaugosKiekiai) <= 500 or 
Planas.planoID = 9 and Max(paslaugosKiekiai) between 501 and 1000 or
Planas.planoID = 11 and Max(paslaugosKiekiai) > 1000)
when Max(paslaugosKiekiai) = coalesce(dbo.udf_V76_kiekis3MenNet(@id),0) then
(Select Operatorius.operatoriausPavadinimas + Planas.planoPavadinimas from Planas
inner join Operatorius on Planas.operatoriausID = Operatorius.operatoriausID
where Planas.planoID = 3 and Max(paslaugosKiekiai) <= 900 or
Planas.planoID = 4 and Max(paslaugosKiekiai) between 901 and 3072 or
Planas.planoID = 17 and Max(paslaugosKiekiai) between 3073 and 3584 or
Planas.planoID = 10 and Max(paslaugosKiekiai) between 3585 and 4608 or
Planas.planoID = 5 and Max(paslaugosKiekiai) between 4609 and 6144 or
Planas.planoID = 18 and Max(paslaugosKiekiai) between 6145 and 9216 or
Planas.planoID = 6 and Max(paslaugosKiekiai) between 9217 and 12288 or
Planas.planoID = 19 and Max(paslaugosKiekiai) > 12288)
when Max(paslaugosKiekiai) = coalesce(dbo.udf_V76_kiekis3MenSmsU(@id),0) then
(Select Operatorius.operatoriausPavadinimas + Planas.planoPavadinimas from Planas
inner join Operatorius on Planas.operatoriausID = Operatorius.operatoriausID
where Planas.planoID = 2 and Max(paslaugosKiekiai) <= 500 or
Planas.planoID = 11 and Max(paslaugosKiekiai) between 501 and 1000 or
Planas.planoID = 20 and Max(paslaugosKiekiai) > 1000)
when Max(paslaugosKiekiai) = coalesce(dbo.udf_V76_kiekis3MenMms(@id),0) then
(Select Operatorius.operatoriausPavadinimas + Planas.planoPavadinimas from Planas
inner join Operatorius on Planas.operatoriausID = Operatorius.operatoriausID
where Planas.planoID = 1 and Max(paslaugosKiekiai) <= 50 or
Planas.planoID = 7 and Max(paslaugosKiekiai) between 51 and 60 or
Planas.planoID = 8 and Max(paslaugosKiekiai) between 61 and 100 or
Planas.planoID = 2 and Max(paslaugosKiekiai) between 101 and 150 or
Planas.planoID = 16 and Max(paslaugosKiekiai) between 151 and 200 or
Planas.planoID = 9 and Max(paslaugosKiekiai) > 200)
when Max(paslaugosKiekiai) = coalesce(dbo.udf_V76_trukme3MenSL(@id),0) then 
(Select Operatorius.operatoriausPavadinimas + Planas.planoPavadinimas from Planas
inner join Operatorius on Planas.operatoriausID = Operatorius.operatoriausID
where Planas.planoID = 1 and Max(paslaugosKiekiai) <= 50 or
Planas.planoID = 9 and Max(paslaugosKiekiai) between 51 and 100 or
Planas.planoID = 12 and Max(paslaugosKiekiai) between 101 and 150 or
Planas.planoID = 13 and Max(paslaugosKiekiai) between 151 and 300 or
Planas.planoID = 14 and Max(paslaugosKiekiai) between 301 and 900 or
Planas.planoID = 15 and Max(paslaugosKiekiai) between 901 and 1500 or
Planas.planoID = 16 and Max(paslaugosKiekiai) between 1501 and 2400 or
Planas.planoID = 2 and Max(paslaugosKiekiai) > 2400)
when Max(paslaugosKiekiai) = coalesce(dbo.udf_V76_trukme3MenSV(@id),0) then
(Select Operatorius.operatoriausPavadinimas + Planas.planoPavadinimas from Planas
inner join Operatorius on Planas.operatoriausID = Operatorius.operatoriausID
where Planas.planoID = 1 and Max(paslaugosKiekiai) <= 50 or
Planas.planoID = 9 and Max(paslaugosKiekiai) between 51 and 250 or
Planas.planoID = 16 and Max(paslaugosKiekiai) between 251 and 500 or
Planas.planoID = 2 and Max(paslaugosKiekiai) > 500)
when Max(paslaugosKiekiai) = coalesce(dbo.udf_V76_trukme3MenSU(@id),0) then
(Select Operatorius.operatoriausPavadinimas + Planas.planoPavadinimas from Planas
inner join Operatorius on Planas.operatoriausID = Operatorius.operatoriausID
where Planas.planoID = 1 and Max(paslaugosKiekiai) <= 50 or
Planas.planoID = 2 and Max(paslaugosKiekiai) between 51 and 100 or
Planas.planoID = 16 and Max(paslaugosKiekiai) between 101 and 150 or
Planas.planoID = 20 and Max(paslaugosKiekiai) > 150)
else 'Plano pasi�lyti negalima.'
END
from dbo.udf_V76_paslauguKiekiai(@id));
END;

Select dbo.udf_V76_MAIN(3098) as siulomasGeriausiaiTinkantisMokejimoPlanas;

Select * from planas inner join Abonentas on Planas.planoID = Abonentas.planoID
where Abonentas.abonentoID = 3098

Drop function udf_V76_MAIN;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V76_paslauguKiekiai (@id int)
Returns table
as
return
(Select
(Select coalesce(dbo.udf_V76_kiekis3MenSms(@id),0)) as paslaugosKiekiai
union all
(Select coalesce(dbo.udf_V76_kiekis3MenNet(@id),0)) 
union all
(Select coalesce(dbo.udf_V76_kiekis3MenSmsU(@id),0))
union all
(Select coalesce(dbo.udf_V76_kiekis3MenMms(@id),0))
union all
(Select coalesce(dbo.udf_V76_trukme3MenSL(@id),0))
union all
(Select coalesce(dbo.udf_V76_trukme3MenSV(@id),0))
union all
(Select coalesce(dbo.udf_V76_trukme3MenSU(@id),0))
);

Select * from dbo.udf_V76_paslauguKiekiai(349);

Drop function udf_V76_paslauguKiekiai;

----------------------------------------------------------------------------------------------------------------------------

BEGIN--KIEKIAI

Create function dbo.udf_V76_kiekis3MenSms (@id int) 
Returns float
as
Begin
Return (Select count(Abonentas.abonentoID)
from Abonentas
left join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
left join UzsakytaLengvata on Abonentas.abonentoID = UzsakytaLengvata.abonentoID
left join UzsakomaLengvata on UzsakytaLengvata.komplektoID = UzsakomaLengvata.komplektoID
left join PlanoRinkinys on UzsakomaLengvata.rinkinioID = PlanoRinkinys.rinkinioID
where RysysP2P.paslaugosID = 2 and RysysP2P.abonentoID = @id 
and RysysP2P.rysioPradzia between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP 
and RysysP2P.rysioPabaiga between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP
);
END;

Select dbo.udf_V76_kiekis3MenSms(349);

Drop function udf_V76_kiekis3MenSms;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V76_kiekis3MenNet (@id int) 
Returns float
as
Begin
Return (Select sum(RysysInternetu.kiekisMB)
from Abonentas
inner join RysysInternetu on Abonentas.abonentoID = RysysInternetu.abonentoID
inner join UzsakytasInternetoPlanas on Abonentas.abonentoID = UzsakytasInternetoPlanas.abonentoID
inner join InternetoPlanas on UzsakytasInternetoPlanas.internetoPlanoID = InternetoPlanas.internetoPlanoID
where Abonentas.abonentoID = @id
and RysysInternetu.rysioPradzia between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP 
and RysysInternetu.rysioPabaiga between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP
);
END;

Select dbo.udf_V76_kiekis3MenNet(12);

Drop function udf_V76_kiekis3MenNet;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V76_kiekis3MenSmsU (@id int) 
Returns float
as
Begin
Return (Select count(Abonentas.abonentoID)
from Abonentas
left join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
left join UzsakytaLengvata on Abonentas.abonentoID = UzsakytaLengvata.abonentoID
left join UzsakomaLengvata on UzsakytaLengvata.komplektoID = UzsakomaLengvata.komplektoID
left join PlanoRinkinys on UzsakomaLengvata.rinkinioID = PlanoRinkinys.rinkinioID
where RysysP2P.paslaugosID = 5 and RysysP2P.abonentoID = @id
and RysysP2P.rysioPradzia between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP 
and RysysP2P.rysioPabaiga between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP
);
END;

Select dbo.udf_V76_kiekis3MenSmsU(349);

Drop function udf_V76_kiekis3MenSmsU;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V76_kiekis3MenMms (@id int) 
Returns float
as
Begin
Return (Select count(Abonentas.abonentoID)
from Abonentas
left join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
left join UzsakytaLengvata on Abonentas.abonentoID = UzsakytaLengvata.abonentoID
left join UzsakomaLengvata on UzsakytaLengvata.komplektoID = UzsakomaLengvata.komplektoID
left join PlanoRinkinys on UzsakomaLengvata.rinkinioID = PlanoRinkinys.rinkinioID
where RysysP2P.paslaugosID = 3 and RysysP2P.abonentoID = @id
and RysysP2P.rysioPradzia between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP 
and RysysP2P.rysioPabaiga between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP
);
END;

Select dbo.udf_V76_kiekis3MenMms(349);

Drop function udf_V76_kiekis3MenMms;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V76_trukme3MenSL (@id int) 
Returns float
as
Begin
Return (Select sum(ceiling(cast(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)as float)/60))
from Abonentas
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 1 and RysysP2P.abonentoID = @id
and RysysP2P.rysioPradzia between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP 
and RysysP2P.rysioPabaiga between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP
);
END;

Select dbo.udf_V76_trukme3MenSL(349);

Drop function udf_V76_trukme3MenSL;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V76_trukme3MenSV (@id int) 
Returns float
as
Begin
Return (Select sum(ceiling(cast(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)as float)/60))
from Abonentas
inner join RysysP2P on Abonentas.abonentoID = RysysP2P.abonentoID
where RysysP2P.paslaugosID = 6 and RysysP2P.abonentoID = @id
and RysysP2P.rysioPradzia between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP 
and RysysP2P.rysioPabaiga between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP
);
END;

Select dbo.udf_V76_trukme3MenSV(349);

Drop function udf_V76_trukme3MenSV;

----------------------------------------------------------------------------------------------------------------------------
Create function dbo.udf_V76_trukme3MenSU (@id int) 
Returns float
as
Begin
Return (Select sum(ceiling(cast(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)as float)/60))
from RysysP2P
where RysysP2P.paslaugosID = 4 and RysysP2P.abonentoID = @id 
and RysysP2P.rysioPradzia between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP 
and RysysP2P.rysioPabaiga between DATEADD(MONTH,-3,CURRENT_TIMESTAMP) and CURRENT_TIMESTAMP
);
END;

Select dbo.udf_V76_trukme3MenSU(349);

Drop function udf_V76_trukme3MenSU;

END--KIEKIAI


BEGIN--TEST6

select DATEADD(MONTH,-3,CURRENT_TIMESTAMP)

Select sum(ceiling(cast(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)as float)/60))
from RysysP2P
where RysysP2P.paslaugosID = 4 and RysysP2P.abonentoID = 349 
and YEAR(RysysP2P.rysioPradzia) = 2014 and MONTH(RysysP2P.rysioPradzia) = 9 and DAY(RysysP2P.rysioPradzia) between 9 and 30

Select sum(ceiling(cast(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)as float)/60))
from RysysP2P
where RysysP2P.paslaugosID = 4 and RysysP2P.abonentoID = 349 
and YEAR(RysysP2P.rysioPradzia) = 2014 and MONTH(RysysP2P.rysioPradzia) = 9 and DAY(RysysP2P.rysioPradzia) between 1 and 8

Select sum(ceiling(cast(DATEDIFF(SECOND,RysysP2P.rysioPradzia, RysysP2P.rysioPabaiga)as float)/60))
from RysysP2P
where RysysP2P.paslaugosID = 4 and RysysP2P.abonentoID = 349 
and YEAR(RysysP2P.rysioPradzia) = 2014 and MONTH(RysysP2P.rysioPradzia) = 10

Select DATEDIFF(second,rysioPradzia,rysioPabaiga)
from RysysP2P
where abonentoID = 349 and paslaugosID = 4 and YEAR(RysysP2P.rysioPradzia) = 2014 and MONTH(RysysP2P.rysioPradzia) = 9

Select *
from RysysP2P
where RysysP2P.paslaugosID = 2 and RysysP2P.abonentoID = 349 
and YEAR(RysysP2P.rysioPradzia) = 2014 and MONTH(RysysP2P.rysioPradzia) = 9 and DAY(RysysP2P.rysioPradzia) between 9 and 30

Select *
from RysysP2P
where RysysP2P.paslaugosID = 2 and RysysP2P.abonentoID = 349 
and YEAR(RysysP2P.rysioPradzia) = 2014 and MONTH(RysysP2P.rysioPradzia) = 12

END--TEST6


